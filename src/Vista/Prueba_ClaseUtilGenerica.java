/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.Fraccion;
import Util.ManejoVector;
/**
 *
 * @author madar
 */
public class Prueba_ClaseUtilGenerica {
    
    public static void main(String[] args) {
        String nombres[]={"andres","jose","yulieth","deisler"};
        Integer codigos[]={1152030,1152031,1152032,1152033};
        Integer codigos2[]={1152030,1152031,1152032,1152033};
        Float notas[]={3.5F,5F,4.3F,5F};
        
        Fraccion fracciones[]={new Fraccion(3,5), new Fraccion(4,7)};
        Fraccion fracciones2[]={new Fraccion(3,5), new Fraccion(4,7)};
        
        ManejoVector<String> v1=new ManejoVector(nombres);
        ManejoVector<Integer> v2=new ManejoVector(codigos);
        ManejoVector<Float> v3=new ManejoVector(notas);
        ManejoVector<Fraccion> v4=new ManejoVector(fracciones);
        ManejoVector<Integer> v5=new ManejoVector(codigos2);
        ManejoVector<Fraccion> v6=new ManejoVector(fracciones2);
        
        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v3.toString());
        System.out.println(v4.toString());
        System.out.println("Son iguales codigos vs codigos2:"+v2.equals(v5));
        System.out.println("Son iguales fracciones vs fracciones2:"+v4.equals(v6));
        
        v1.ordenarPorSeleccion();
        v2.ordenarPorSeleccion();
        v3.ordenarPorSeleccion();
        v4.ordenarPorSeleccion();
        v5.ordenarPorSeleccion();
        v6.ordenarPorSeleccion();
        
        System.out.println("Vectores ordenados:");
        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v3.toString());
        System.out.println(v4.toString());
        System.out.println(v5.toString());
        System.out.println(v6.toString());
        
        
        
    }
    
}
